# PYVERLAYER!

![](https://gitlab.com/fatualux/pyverlayer/-/raw/main/static/demo.gif?ref_type=heads)

### This is basic app to put an image over a batch of images using Python and Flask.

## INSTALLATION

```
git clone https://www.gitlab.com:fatualux/pyverlayer && cd pyverlayer
```

You can install all the needed dependencies with the following command:

```
python -m pip install -r requirements.txt
```

## USAGE

```
python app.py
```

Then, open your preferred browser and go to http://127.0.0.1:5000/

Done!

## LICENSE

[![License](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

This project is licensed under the GPLv3 license.
See LICENSE file for more details.
