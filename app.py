from flask import Flask, render_template, request, redirect, flash
import os
import shutil
from PIL import Image

app = Flask(__name__)
app.secret_key = 'your_secret_key'

# Define directory paths
ORIGINAL_DIR = 'ORIGINAL'
FRAME_DIR = 'FRAME'
RESULT_DIR = 'RESULT'


# Function to overlay frame on original images
def overlay_frame_on_images(original_dir, frame_img_path, result_dir):
    # Create result directory if it doesn't exist
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    # Open frame image and convert to RGBA mode
    frame_img = Image.open(frame_img_path)
    if frame_img.mode != 'RGBA':
        frame_img = frame_img.convert('RGBA')

    # Loop through original images
    for filename in os.listdir(original_dir):
        if filename.endswith(".jpg") or filename.endswith(".png"):
            # Open original image
            original_img_path = os.path.join(original_dir, filename)
            original_img = Image.open(original_img_path)

            # Convert original image to RGBA mode
            if original_img.mode != 'RGBA':
                original_img = original_img.convert('RGBA')

            # Resize frame image to match original image size
            frame_img_resized = frame_img.resize(original_img.size)

            # Overlay frame onto original image
            overlayed_img = Image.alpha_composite(original_img,
                                                  frame_img_resized)

            # Save result image
            result_img_path = os.path.join(result_dir, filename)
            overlayed_img.save(result_img_path)

    # Delete original and frame directories
    shutil.rmtree(original_dir)
    shutil.rmtree(os.path.dirname(frame_img_path))

    return result_dir


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def upload():
    if 'original_imgs' not in request.files \
            or 'frame_img' not in request.files:
        flash('Please select both original images and a frame image.')
        return redirect(request.url)

    original_imgs = request.files.getlist('original_imgs')
    frame_img = request.files['frame_img']

    if len(original_imgs) == 0 or frame_img.filename == '':
        flash('Please select both original images and a frame image.')
        return redirect(request.url)

    # Create ORIGINAL directory if it doesn't exist
    original_dir = os.path.join(app.root_path, ORIGINAL_DIR)
    if not os.path.exists(original_dir):
        os.makedirs(original_dir)

    # Save original images to ORIGINAL directory
    for img in original_imgs:
        img.save(os.path.join(original_dir, img.filename))

    # Create FRAME directory if it doesn't exist
    frame_dir = os.path.join(app.root_path, FRAME_DIR)
    if not os.path.exists(frame_dir):
        os.makedirs(frame_dir)

    # Save frame image to FRAME directory
    frame_img_path = os.path.join(frame_dir, frame_img.filename)
    frame_img.save(frame_img_path)

    # Overlay frame on original images and save to RESULT directory
    result_dir = overlay_frame_on_images(original_dir,
                                         frame_img_path,
                                         os.path.join(app.root_path,
                                                      RESULT_DIR))

    return render_template('result.html', result_dir=result_dir)


if __name__ == '__main__':
    app.run(debug=True)
